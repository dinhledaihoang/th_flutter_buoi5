import 'package:my_plan/repository/repository.dart';
import '../model/task.dart';
import '../model/plan.dart';
import 'package:my_plan/repository/in_memory_cache.dart';

class PlanService {
  Repository _repository = InMemoryCache();

  Plan createPlan(String name) {
    final model = _repository.create();
    final plan = Plan.fromModel(model)..name = name;
    savePlan(plan);
    return plan;
  }

  void savePlan(Plan plan) {
    _repository.update(plan.toModel());
  }

  void delete(Plan plan) {
    _repository.delete(plan.toModel().id);
  }

  List<Plan> getAllPlan() {
    return _repository.getAll().map<Plan>((model) => Plan.fromModel(model)).toList();
  }

  void addTask(Plan plan, String description) {
    print('plan.task=${plan.task}');
    print('plan.task.last=${plan.task.length}');
    final id = (plan.task.length > 0) ? plan.task.last.id : 1;
    final task = Task(id: id, description: description);
    plan.task.add(task);
    savePlan(plan);
  }

  void deleteTask(Plan plan, Task task) {
    plan.task.remove(task);
    savePlan(plan);
  }
}