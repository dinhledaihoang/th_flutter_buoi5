import 'package:flutter/material.dart';
import 'package:my_plan/plan_provider.dart';
import 'view/app.dart';
import 'plan_provider.dart';

void main() {
  var planProvider = PlanProvider(child: MyPlanApp());
  runApp(planProvider);
}