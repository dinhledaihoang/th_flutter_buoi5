import '../service/plan_service.dart';
import '../model/data_layer.dart';

class PlanController {
  final service = PlanService();

  List<Plan> get plan => List.unmodifiable(service.getAllPlan());

  void addNewPlan(String name) {
    if (name.isEmpty) {
      return;
    }
    name = _checkForDuplicates(plan.map((plan) => plan.name), name);
    service.createPlan(name);
  }

  void savePlan(Plan plan) {
    service.savePlan(plan);
  }

  void deletePlan(Plan plan) {
    service.delete(plan);
  }

  void createNewTask(Plan plan, [String? description]) {
    if (description == null || description.isEmpty) {
      description = 'New Task';
    }

    description = _checkForDuplicates(
        plan.task.map((task) => task.description), description);

    service.addTask(plan, description);
  }

  void deleteTask(Plan plan, Task task) {
    service.deleteTask(plan, task);
  }

  String _checkForDuplicates(Iterable<String> items, String text) {
    final duplicatedCount = items.where((item) => item.contains(text)).length;
    if (duplicatedCount > 0) {
      text += ' ${duplicatedCount + 1}';
    }
    return text;
  }
}