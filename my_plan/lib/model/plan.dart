import 'package:my_plan/repository/repository.dart';
import 'package:flutter/foundation.dart';
import 'task.dart';

class Plan {
  int id = 0;
  String name = '';
  List<Task> task = [];

  Plan({required this.id, this.name = ''});

  Plan.fromModel(Model model) {
    id = model.id;
    name = model.data['name'] ?? '';
    if (model.data['task'] != null) {
      task = model.data['task'].map<Task>((task) => Task.fromModel(task)).toList();
    }
  }

  Model toModel() => Model(id: id, data: {
    'name': name,
    'task': task.map((task) => task.toModel()).toList()
  });

  int get completeCount => task.where((task) => task.complete).length;

  String get completenessMessage =>
      '$completeCount out of ${task.length} tasks';
}